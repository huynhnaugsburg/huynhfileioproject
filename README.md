WordCounter Program ver2
This program will take in a given word document and count the number of words that appear within.
It will also then generate a file that will have the count for each word.
A GUI will appear where all of these function can be called and used.
----------------------
Getting Started & Running:
1.	Open a Terminal window and path your way to the folder where WordCounter.java exists.
2.	Run the folling commands inside of the terminal window:
	
	javac WordCounter.java
	java WordCounter

3.After running the terminal commands a GUI will appear.
4. Use the Choose File button to pick a file.
5. Then use the Count Words button to begin counting the words within the chose file.
6. The program will inform where the newly created "WordCount.txt" was created.
7. You can close the program with the Quit button or simply close the window with the x.

----------------------
Built With:
Java - Used to code project

----------------------
Author:
Nghia Huynh - Code and Stuff - Augsburg University

----------------------
License:
This project is license under nobody and is free to use for all.

----------------------
Acknowledgements:
Google - Telling me what to do.

