import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * WriteToFile.java
 * Compilers 1
 * September 28, 2018
 * This class is used to take in given hashmap and then formats and writes the information to a .txt file.
 * @author Nghia Huynh
 * @version 2
 */
public class WriteToFile {
	
	/**
	 * This method takes in a hashmap and a total count of the words to produce a "WordCount.txt" file that the user can view.
	 * @param wordhashmap The hashmap that the caller wants printed, the keys must be strings and the values must be ints.
	 * @param totalcount The value that the user gives that is the total number of words.
	 * @return path The absolute path of the outputed "WordCount.txt" file.
	 */
	public static String writeData(HashMap<String,Integer> wordhashmap,int totalcount)
	{
		/*
		 * The arraylist is used below to allow the sorting of the given hashmap. 
		 * Collections is used to sort said arraylist.
		 */
		ArrayList<String> wordlist = new ArrayList<String>();
		//This for loop is used to loop through the hashmap and adds every key to an arraylsit.
		for (Entry<String, Integer> key : wordhashmap.entrySet())
		{
		     wordlist.add(key.getKey());
		}
		Collections.sort(wordlist);
		
		/*
		 * Creates the files output stream and buffered writer to begin writing to the file.
		 */
		FileOutputStream fileOutput = null;
		BufferedWriter fileWriter = null;
		
		/*
		 * This try catch is used to check if a file can be created.
		 */
		try
		{
			fileOutput = new FileOutputStream("WordCount.txt");
			fileWriter = new BufferedWriter(new OutputStreamWriter(fileOutput));
		}
		catch (FileNotFoundException e)
		{
			/*
			 * This exception closes the code if it could not write to a file.
			 */
			System.out.println("Could not create WordCount.txt! Closing program!");
			System.exit(0);
		}
		
		/*
		 * This try catch block checks if any of the lines inside can be written to a file.
		 */
		try 
		{
			/*
			 * The lines below are used to format the begging of what will be written to file.
			 */
			fileWriter.write(String.format("%-15s|%-15s\n","Word", "Appearance"));
		    fileWriter.newLine();
			fileWriter.write(String.format("%-15s|%-15s\n","Total Count", totalcount));
		    fileWriter.newLine();
			fileWriter.write("-------------------------");
		    fileWriter.newLine();
		    /*
		     * This for loop uses the sorted arraylist and loops through it printing the each word string and its count to the 
		     * "WordCount.txt" file.
		     */
			for (int i = 0;i < wordhashmap.size();i++) 
			{
			    fileWriter.write(String.format("%-15s|%-15s\n",wordlist.get(i), wordhashmap.get(wordlist.get(i)).toString()));
			    fileWriter.newLine();
			}
			/*
			 * These lines close the file writer.
			 */
			fileWriter.flush();
			fileWriter.close();
		}
		/*
		 * This exception is for when no file was written to.
		 */
		catch (IOException e)
		{
			System.out.println("Could not write to file! Closing program!");
			System.exit(0);
		}
		//These lines gets the absolute file path of the output file and returns it to the user.
		String path =  new String(new File("WordCount.txt").getAbsolutePath());
		return path;
		
	}
}
