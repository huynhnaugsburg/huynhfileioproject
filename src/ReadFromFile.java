import java.util.Scanner;
import java.util.Map.Entry;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.HashMap;

/**
 * ReadFromFile.java
 * Compilers 1
 * September 28, 2018
 * This class is used to read a file and add string to a hashmap that the caller can then use.
 * @author Nghia Huynh
 * @version 2
 */
public class ReadFromFile {
	
	/**
	 * This method takes in a hasmap that has strings for keys and ints for values 
	 * and sums up all the values to get a total.
	 * @param table The hashmap that will have its int values added up.
	 * @return total The total value of all the int values.
	 */
	public static int countTotalWords(HashMap<String, Integer> table)
	{
		int total = 0;
		for (Entry<String, Integer> key : table.entrySet()) 
		{
		     total = total + key.getValue();
		}
		
		return total;
		
	}
	
	
	/**
	 * This method takes in a file object and counts the number of words and their occurrences.
	 * It then uses the words as keys and occurrences as values to create a hashmap
	 * @param myfile The file that the caller wants to be hashmaped
	 * @return wordtable Returns the created hashmap to the caller.
	 */
	public static HashMap<String, Integer> readFile(File myfile)
	{
		Scanner scanner = null;							//Creates a scanner.
		HashMap<String,Integer> wordtable = new HashMap<>();
		/*
		 * This try catch is used to check if the file inputed can be scanned from.
		 */
		try
		{
			scanner = new Scanner(myfile);				//Set scanner to read the internal file.
		}
		/*
		 * If a file was not found the error is displayed to the user.
		 */
		catch(FileNotFoundException e)
		{
			System.out.println("No file was found! Closing program!");
			System.exit(0);
		}
		
		/*
		 * The while loop is used to read the file string by string until there are none left in the file.
		 */
		while (scanner.hasNext())
		{
			String word = scanner.next();			//Scan next string in the file.
			/*
			 * The if statement is used to check if a two words are connected by a - and then splits the them if it exists.
			 */
			if(word.contains("-"))
			{
				String[] pieces = word.split("-");	//Stores the split words in a string array.
				/*
				 * The for loop is used to iterate over the string array and add the words to the hashmap.
				 */
				for(int i = 0;i < pieces.length;i++)
				{
					wordtable = addToHashMap(wordtable, pieces[i]);
				}
			}
			else
			{
				wordtable = addToHashMap(wordtable, word);
			}
		}
		wordtable.remove("");//This line removes instances where a form of punctuation was surrounded by white space, but had no words near it.
		
		scanner.close();							//Closes the scanner.
		
		return wordtable;
	}
	
	/**
	 * This method is used to remove the punctuation from the beginning and end of a given string, but leaves all punctuation inside of a
	 * word. It will also turn any capital letters to lower case. After it does those tasks it adds it to the given hashmap.
	 * @param table a given hashmap that the word will be added to.
	 * @param word The string that the method will edit and then added to a hashmap
	 * @return table This returns the orignal table with the new word string added.
	 */
	public static HashMap<String, Integer> addToHashMap(HashMap<String, Integer> table, String word)
	{
		word = word.replaceAll("^\\p{Punct}+", "");		//Removes the punctuation from the begging of a word.
		word = word.replaceAll("\\p{Punct}+$", "");		//Removes the punctuation from the end of a word.
		word = word.toLowerCase();						//Swaps upper case letters for lower case ones
		/*
		 * This if loop checks to see if the word already exists in the hashmap.
		 * If it exists in the hashmap it increases the value by 1.
		 * Else it does not exists it adds a new key and sets it to 1.
		 * Both results increase the total word count by 1.
		 */
		if(table.containsKey(word) == true)
		{
			int count = table.get(word);
			table.put(word, count+1);
		}
		else if(table.containsKey(word) == false)
		{
			table.put(word, 1);
		}
		return table;
	}
}
