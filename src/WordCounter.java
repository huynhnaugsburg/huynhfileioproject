/**
 * WordCounter.java
 * Compilers 1
 * September 28, 2018
 * This is the program main that will be used to invoke the code and run it.
 * @author Nghia Huynh
 * @version 2
 */
public class WordCounter {
	public static void main(String[] args) {
		//The lines below create a new GUI object and makes it visible.
		WordCounterGUI newwindow = new WordCounterGUI();
		newwindow.setVisible(true);
	}

}
