import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;




/**
 * WriteToFile.java
 * Compilers 1
 * September 28, 2018
 * This class is used to display a gui to the user to run the WordCounter program.
 * @author Nghia Huynh
 * @version 1
 */
	@SuppressWarnings("serial")
	public class WordCounterGUI extends JFrame implements ActionListener {
		private JLabel status = new JLabel("Status: Waiting for chosen file.");	//Tells the user the status of the program
		private JLabel filepath = new JLabel("In File Path: No file");			//Shows the files path of either the entered or output file.
		private JLabel quitguide = new JLabel("Hit the Quit button or close the window to exit the program!");//Tells user how to quit program.
		private JButton filechoosebutton = new JButton("Choose file!");			//The button that a user uses to choose a file.
		private JButton filecountbutton = new JButton("Count Words");			//Button to activate word counting code.
		private JButton quitbutton = new JButton("Quit");						//Button quits the code.
		private File chosenfile = null;											//Holds the global file for the GUI class.
		
		public WordCounterGUI() {
			//Layout of main window.
			this.setTitle("WordCounter");
			this.setBounds(300, 300, 600, 200);
			this.getContentPane().setLayout(null);
			//Layout for status text.
			this.status.setBounds(10, 35, 600 , 100);
			this.getContentPane().add(status);
			//Layout for filepath text.
			this.filepath.setBounds(10, 50, 600 , 100);
			this.getContentPane().add(filepath);
			//layout for quitguide text.
			this.quitguide.setBounds(10, 90, 600 , 100);
			this.getContentPane().add(quitguide);
			//layout for filechoose button.
			this.filechoosebutton.setBounds(10, 5, 280, 50);
			this.getContentPane().add(filechoosebutton);
			this.filechoosebutton.addActionListener(this);
			//layout for filecount button.
			this.filecountbutton.setBounds(295, 5, 280, 50);
			this.getContentPane().add(filecountbutton);
			this.filecountbutton.addActionListener(this);
			//Layout for quit button.
			this.quitbutton.setBounds(515, 120, 60, 30);
			this.getContentPane().add(quitbutton);
			this.quitbutton.addActionListener(this);
			
			//Closes program if user hits the x on the window to close it.
			this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		}
		
		/* This code is used to check which button has been pressed and preforms an action based on that information.
		 * (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//If the filechoosebutton is pressed if will then open up a file choose ui and update the status of the code.
			if(e.getSource() == filechoosebutton)
			{
				this.filechoosebutton.setText("Pushed!");
				JFileChooser file = new JFileChooser();
				//This filter makes sure users do not pick documents that are not text.
				file.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
				int filefound = file.showOpenDialog( null);
				chosenfile = file.getSelectedFile();
				//This if checks if a file has been chosen if not it informs the user.
				if(filefound == JFileChooser.APPROVE_OPTION)
				{
					this.status.setText("File has been chosen!");
					this.filepath.setText("In File Path: " + chosenfile.getAbsolutePath());
				}
				else
				{
					this.status.setText("Status: File choosing window was closed! Choose a file!");		
				}
				this.filechoosebutton.setText("Choose file!");
			}
			//If the filecountbutton is pushed and there is file in the class then it will run the WordCounter code and create an output file.
			if(e.getSource() == filecountbutton && chosenfile != null)
			{
					this.status.setText("Status: Working on file!");
					HashMap<String, Integer> myHashMap = new HashMap<>(ReadFromFile.readFile(chosenfile)); //Reads in file and puts its info into new hashmap
					int total = ReadFromFile.countTotalWords(myHashMap);								   //Get total number of words from the hashmap
					this.filepath.setText("Out File Path: " + WriteToFile.writeData(myHashMap, total));	   //Calls method to write to a file and tells user where the output file is.
					this.status.setText("Counting is complete! Please look at WordCount.txt");			   //Informs user the code is done.
			}
			//If user pushes filecountbutton before a file is chosen it will warn them.
			if(e.getSource() == filecountbutton && chosenfile == null)
			{
				this.status.setText("Status: No file has been chosen! Choose a file first!");
			}
			//If the quit button is pressed the program will exit.
			if(e.getSource() == quitbutton)
			{
				System.out.println("Quit was pushed! Good bye!");
				System.exit(0);
			}
	}
}